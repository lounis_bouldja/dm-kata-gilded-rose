import { expect } from "chai";
import { MyGildedRose } from "@/my-gilded-rose";
import { Item } from "@/item.dto";

describe("Gilded Rose", () => {
  it("should foo", () => {
    const gildedRose = new MyGildedRose([new Item("foo", 0, 0)]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).to.equal("fixme");
  });
});
