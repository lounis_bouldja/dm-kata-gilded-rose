import { ItemFactory } from "@/item.factory";
import { AgedBrieItem } from "@/domain/item/aged-brie-item";
import { ConjuredItem } from "@/domain/item/conjured-item";
import { SulfurasItem } from "@/domain/item/sulfuras-item";
import { StandardItem } from "@/domain/item/standard-item";
import { Item } from "@/item.dto";

describe("Item factory", () => {
  it("Create ConjuredItem", function () {
    const props = { name: "Conjured item test", sellIn: 10, quality: 120 };
    expect(ItemFactory.of(props)).toBeInstanceOf(ConjuredItem);
  });

  it("Create AgedBrieItem", function () {
    const props = { name: "Aged Brie", sellIn: 10, quality: 120 };
    expect(ItemFactory.of(props)).toBeInstanceOf(AgedBrieItem);
  });

  it("Create SulfurasItem", function () {
    const props = {
      name: "Sulfuras, Hand of Ragnaros",
      sellIn: -1,
      quality: 80,
    };
    expect(ItemFactory.of(props)).toBeInstanceOf(SulfurasItem);
  });

  it("Create StandardItem", function () {
    const props = {
      name: "Standard item test",
      sellIn: 10,
      quality: 120,
    };
    expect(ItemFactory.of(props)).toBeInstanceOf(StandardItem);
  });

  it("Map to item", function () {
    const properties = {
      name: "Standard item test",
      sellIn: 10,
      quality: 20,
    };
    const standardItem = new StandardItem(properties);
    const actual = ItemFactory.mapToItem(standardItem);
    expect(actual).toBeInstanceOf(Item);
    expect(actual).toEqual(properties);
  });
});
