import { Quality } from "@/domain/item/vo/quality";

describe("Quantity", () => {
  it("Create valide quantity", () => {
    // given
    const expected =
      (Quality.DEFAULT_MAX_VALUE + Quality.DEFAULT_MIN_VALUE) / 2;
    // when
    const qnt = new Quality(expected);
    // then
    expect(qnt.value).toBe(expected);
  });

  it("A item can never have its Quality above 50", () => {
    // given when
    const qnt = new Quality(70);
    // then
    expect(qnt.value).toBe(Quality.DEFAULT_MAX_VALUE);
  });

  it("A item can never have its Quality increase above 50", () => {
    // given
    const qnt = new Quality(Quality.DEFAULT_MAX_VALUE - 1);
    // when
    qnt.increase(2);
    // then
    expect(qnt.value).toBe(Quality.DEFAULT_MAX_VALUE);
  });

  it("A item can never have its Quality under 0", () => {
    // given when
    const qnt = new Quality(-1);
    // then
    expect(qnt.value).toBe(Quality.DEFAULT_MIN_VALUE);
  });

  it("A item can never have its Quality decrease under 0", () => {
    // given
    const qnt = new Quality(Quality.DEFAULT_MIN_VALUE + 1);
    // when
    qnt.decrease(2);
    // then
    expect(qnt.value).toBe(Quality.DEFAULT_MIN_VALUE);
  });

  it("Set minimum value", () => {
    // given
    const qnt = new Quality(1);
    // when
    qnt.setMinimumValue();
    // expected
    expect(qnt.value).toBe(Quality.DEFAULT_MIN_VALUE);
  });

  it("Create quantity with valid min and max", () => {
    // given
    const maxValue = 80;
    const minValue = 0;
    // when
    const qnt = new Quality(1, minValue, maxValue);
    // expected
    expect(qnt.maxValue).toBe(maxValue);
    expect(qnt.minValue).toBe(minValue);
  });

  it("Create quantity with not valid min and max", () => {
    // given
    const maxValue = 0;
    const minValue = 50;
    // when
    const qnt = new Quality(1, minValue, maxValue);
    // expected
    expect(qnt.maxValue).toBe(Quality.DEFAULT_MAX_VALUE);
    expect(qnt.minValue).toBe(Quality.DEFAULT_MIN_VALUE);
  });
});
