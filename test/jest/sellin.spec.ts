import { SellIn } from "@/domain/item/vo/sell-in";

describe("SellIn", () => {
  it("Create valide sellIn", () => {
    // given
    const expected = 1;
    // when
    const sellIn = new SellIn(expected);

    expect(sellIn.getValue()).toBe(expected);
  });

  it("decrease sellIn", () => {
    // given
    const expected = 1;
    // when
    const sellIn = new SellIn(2);
    sellIn.decrease(1);
    //then
    expect(sellIn.getValue()).toBe(expected);
  });

  it("is Over, is Under", () => {
    // given
    // when
    const sellIn = new SellIn(2);
    // then
    expect(sellIn.isOver(0)).toBe(true);
    expect(sellIn.isUnder(10)).toBe(true);
  });
});
