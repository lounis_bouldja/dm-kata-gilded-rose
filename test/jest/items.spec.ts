import {StandardItem} from '@/domain/item/standard-item';
import {AgedBrieItem} from '@/domain/item/aged-brie-item';
import {SulfurasItem} from '@/domain/item/sulfuras-item';
import {BackStagePassItem} from '@/domain/item/back-stage-pass-item';
import {ConjuredItem} from '@/domain/item/conjured-item';

describe('item spec', () => {
  it('À la fin de chaque journée, notre système diminue ces deux valeurs pour chaque produit', () => {
    //given
    const item = createStandardItem();
    const expectedQuantity = item.getQualityValue() - 1;
    const expectedSellInd = item.getSellInValue() - 1;
    // when
    item.update();
    // then
    expect(item.getSellInValue()).toBe(expectedSellInd);
    expect(item.getQualityValue()).toBe(expectedQuantity);
  });

  it('Aged Brie augmente sa qualité (quality) plus le temps passe', () => {
    //given
    const item = createAgedBrieItem();
    const expectedQuantity = item.getQualityValue() + 1;
    const expectedSellInd = item.getSellInValue() - 1;
    // when
    item.update();
    // then
    expect(item.getSellInValue()).toBe(expectedSellInd);
    expect(item.getQualityValue()).toBe(expectedQuantity);
  });

  it('Sulfuras, étant un objet légendaire, n\'a pas de date de péremption et ne perd jamais en qualité (quality)', () => {
    //given
    const item = createSulfurasItem();

    const expectedQuantity = item.getQualityValue();
    // when
    item.update();
    // then
    expect(item.getQualityValue()).toBe(expectedQuantity);
    expect(item.isLegend()).toBe(true);
  });

  describe('les éléments "Conjured"', () => {
    it('Voient leur qualité se dégrader de deux fois plus vite que les objets normaux', () => {
      //given
      const item = new ConjuredItem({
        name: 'conjured',
        quality: 10,
        sellIn: 20,
      });

      const expectedQuantity = item.getQualityValue() - 2;
      // when
      item.update();
      // then
      expect(item.getQualityValue()).toBe(expectedQuantity);
      expect(item.isExpired()).toBe(false);
    });

    it('la qualité tombe deux fois plus rapide après le concert', () => {
      //given
      const item = new ConjuredItem({
        name: 'conjured',
        quality: 50,
        sellIn: 0,
      });
      const expectedQuantity = item.getQualityValue() - 2 * 2;
      // when
      item.update();
      // then
      expect(item.getQualityValue()).toBe(expectedQuantity);
      expect(item.isExpired()).toBe(true);

    });

  });

  describe('Backstage passes, augmente sa qualité (quality) plus le temps passe (sellIn) ', () => {
    it('La qualité augmente de 1 quand il reste plus 10', function () {
      // given
      const backStage = new BackStagePassItem({
        name: 'back Stage',
        sellIn: 11,
        quality: 20,
      });
      // when
      backStage.update();
      expect(backStage.getQualityValue()).toBe(21);
      expect(backStage.getSellInValue()).toBe(10);
    });

    it('La qualité augmente de 2 quand il reste 10 jours', function () {
      // given
      const backStage = new BackStagePassItem({
        name: 'back Stage',
        sellIn: 10,
        quality: 20,
      });
      // when
      backStage.update();
      expect(backStage.getQualityValue()).toBe(22);
      expect(backStage.getSellInValue()).toBe(9);
    });

    it('La qualité augmente de 3 quand il reste 5 jours', function () {
      // given
      const backStage = new BackStagePassItem({
        name: 'back Stage',
        sellIn: 2,
        quality: 20,
      });
      // when
      backStage.update();
      expect(backStage.getQualityValue()).toBe(23);
      expect(backStage.getSellInValue()).toBe(1);
    });

    it('la qualité tombe à 0 après le concert', function () {
      // given
      const backStage = new BackStagePassItem({
        name: 'back Stage',
        sellIn: 0,
        quality: 20,
      });
      // when
      backStage.update();
      expect(backStage.isExpired()).toBe(true);
      expect(backStage.getQualityValue()).toBe(0);
    });
  });

  function createStandardItem() {
    return new StandardItem({
      name: 'sta item',
      sellIn: 10,
      quality: 10,
    });
  }

  function createAgedBrieItem() {
    return new AgedBrieItem({
      name: 'Aged Brie',
      sellIn: 10,
      quality: 10,
    });
  }

  function createSulfurasItem() {
    return new SulfurasItem('Sulfuras');
  }
});
