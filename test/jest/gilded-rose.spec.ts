import { Item } from "@/item.dto";
import { MyGildedRose } from "@/my-gilded-rose";
import { SellIn } from "@/domain/item/vo/sell-in";
import { GildedRose } from "@/gilded-rose";
import { Quality } from "@/domain/item/vo/quality";

describe("Gilded Rose", () => {
  it("should foo", () => {
    const gildedRose = new MyGildedRose([
      new Item("Sulfuras, Hand of Ragnaros", 0, 0),
    ]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe("Sulfuras, Hand of Ragnaros");
  });

  it("should foo", () => {
    const gildedRose = new MyGildedRose([
      new Item("Sulfuras, Hand of Ragnaros", 0, 0),
    ]);
    const items = gildedRose.updateQuality();
    expect(items[0].name).toBe("Sulfuras, Hand of Ragnaros");
  });

  it("Like golden master text test ", function () {
    const given = [
      new Item("+5 Dexterity Vest", 10, 20),
      new Item("Aged Brie", 2, 0),
      new Item("Elixir of the Mongoose", 5, 7),
      new Item(
        "Sulfuras, Hand of Ragnaros",
        SellIn.INFINITE_VALUE,
        Quality.LEGEND_QUALITY_VALUE
      ),
      new Item(
        "Sulfuras, Hand of Ragnaros",
        SellIn.INFINITE_VALUE,
        Quality.LEGEND_QUALITY_VALUE
      ),
      new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
      new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49),
      new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49),
    ];
    const expected = new GildedRose(given);
    // when, then
    const myGildedRose = new MyGildedRose(given);
    // just fot testing that the new result is the same with the old code
    // PS : without conjured item - for ConjuredItem tests see item.spec
    for (let i = 0; i < 1000; i++) {
      // Not best practice to use except in loop
      expect(myGildedRose.updateQuality()).toStrictEqual(
        expected.updateQuality()
      );
    }
  });
});
