import { ItemBase } from "./item.base";

export class ConjuredItem extends ItemBase {
  protected readonly QUALITY_DECREASE_VALUE = 2;

  update(): void {
    this.sellIn.decrease(1);
    this.quality.decrease(this.QUALITY_DECREASE_VALUE);
    if (this.isExpired()) {
      this.quality.decrease(this.QUALITY_DECREASE_VALUE);
    }
  }
}
