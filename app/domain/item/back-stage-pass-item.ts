import { ItemBase } from "./item.base";

export class BackStagePassItem extends ItemBase {
  update(): void {
    this.sellIn.decrease(1);
    if (this.isExpired()) {
      this.quality.setMinimumValue();
      return;
    }
    this.quality.increase(1);
    if (this.sellIn.isUnder(10)) {
      this.quality.increase(1);
    }
    if (this.sellIn.isUnder(5)) {
      this.quality.increase(1);
    }
  }
}
