import { Quality } from "./vo/quality";
import { SellIn } from "./vo/sell-in";
import { isNumberObject } from "util/types";

export type ItemProperties = Required<{
  readonly name: string;
  readonly sellIn: number | SellIn;
  readonly quality: number | Quality;
}>;

// entity / Aggergate root
export abstract class ItemBase {
  readonly name: string;
  protected sellIn: SellIn;
  protected quality: Quality;
  protected QUALITY_DECREASE_VALUE = 1;

  constructor(properties: ItemProperties) {
    this.name = properties.name;
    this.sellIn =
      typeof properties.sellIn === "number"
        ? new SellIn(properties.sellIn)
        : properties.sellIn;
    this.quality =
      typeof properties.quality === "number"
        ? new Quality(properties.quality)
        : properties.quality;
  }

  public getSellInValue(): number {
    return this.sellIn.getValue();
  }

  public getQualityValue(): number {
    return this.quality.value;
  }

  public isExpired(): boolean {
    return this.sellIn.isExpired();
  }

  getProperties(): ItemProperties {
    return {
      name: this.name,
      sellIn: this.sellIn.getValue(),
      quality: this.quality.value,
    };
  }

  public isLegend() {
    return (
      this.sellIn.getValue() === SellIn.INFINITE_VALUE &&
      this.quality.hasLegendQuality()
    );
  }

  abstract update(): void;
}
