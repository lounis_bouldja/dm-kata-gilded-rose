import { ItemBase } from "./item.base";

export class AgedBrieItem extends ItemBase {
  update(): void {
    this.sellIn.decrease(1);
    this.quality.increase(this.isExpired() ? 2 : 1);
  }
}
