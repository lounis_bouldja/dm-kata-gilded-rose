import { Quality } from "./vo/quality";
import { SellIn } from "./vo/sell-in";
import { ItemBase } from "./item.base";

export class SulfurasItem extends ItemBase {
  constructor(name) {
    super({
      name,
      quality: Quality.LEGEND_QUANTITY,
      sellIn: SellIn.INFINITE_VALUE,
    });
  }

  update(): void {}
}
