import { ItemBase } from "./item.base";

export class StandardItem extends ItemBase {
  update(): void {
    this.sellIn.decrease(1);
    this.isExpired()
      ? this.quality.decrease(this.QUALITY_DECREASE_VALUE * 2)
      : this.quality.decrease(this.QUALITY_DECREASE_VALUE);
  }
}
