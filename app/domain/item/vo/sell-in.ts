export class SellIn {
  public static readonly INFINITE_VALUE = 999999;

  constructor(private value: number) {
    this.value = value;
  }

  public getValue(): number {
    return this.value;
  }

  public decrease(number) {
    this.value -= number;
  }

  isExpired(): boolean {
    return this.isUnder(0);
  }
  isUnder(day: number) {
    return this.value < day;
  }

  isOver(day: number) {
    return this.value > day;
  }
}
