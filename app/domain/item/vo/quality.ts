export class Quality {
  public static readonly DEFAULT_MAX_VALUE = 50;
  public static readonly DEFAULT_MIN_VALUE = 0;
  public static readonly LEGEND_QUALITY_VALUE = 80;
  public static readonly LEGEND_QUANTITY = new Quality(
    Quality.LEGEND_QUALITY_VALUE,
    Quality.LEGEND_QUALITY_VALUE,
    Quality.LEGEND_QUALITY_VALUE
  );

  constructor(
    private _value: number,
    private readonly _minValue = Quality.DEFAULT_MIN_VALUE,
    private readonly _maxValue = Quality.DEFAULT_MAX_VALUE
  ) {
    if (_minValue > _maxValue) {
      console.info("max < min -> reset to default value (0, 50)");
      this._minValue = Quality.DEFAULT_MIN_VALUE;
      this._maxValue = Quality.DEFAULT_MAX_VALUE;
    }
    this._value = this.getValidValue(_value);
  }

  get value(): number {
    return this._value;
  }

  get maxValue(): number {
    return this._maxValue;
  }

  get minValue(): number {
    return this._minValue;
  }

  public decrease(number = 1) {
    this._value = this.getValidValue(this._value - number);
  }

  public increase(number = 1) {
    this._value = this.getValidValue(this._value + number);
  }

  public setMinimumValue() {
    this._value = this._minValue;
  }

  private getValidValue(value: number): number {
    if (value > this._maxValue) {
      return this._maxValue;
    }
    if (value < this._minValue) {
      return this._minValue;
    }
    return value;
  }

  hasLegendQuality() {
    return this === Quality.LEGEND_QUANTITY;
  }
}
