import { SulfurasItem } from "./domain/item/sulfuras-item";
import { AgedBrieItem } from "./domain/item/aged-brie-item";
import { BackStagePassItem } from "./domain/item/back-stage-pass-item";
import { ConjuredItem } from "./domain/item/conjured-item";
import { ItemBase, ItemProperties } from "./domain/item/item.base";
import { StandardItem } from "./domain/item/standard-item";
import { Item } from "./item.dto";

const BRIE = "Aged Brie";
const CONJURED = "Conjured";
const SULFURAS = "Sulfuras, Hand of Ragnaros";
const BACKSTAGE_PASSES_ITEM = "Backstage passes to a TAFKAL80ETC concert";

export class ItemFactory {
  public static of(props: ItemProperties): ItemBase {
    switch (props.name) {
      case SULFURAS:
        return new SulfurasItem(props.name);
      case BRIE:
        return new AgedBrieItem(props);
      case BACKSTAGE_PASSES_ITEM:
        return new BackStagePassItem(props);
      default:
        return ItemFactory.isConjured(props)
          ? new ConjuredItem(props)
          : new StandardItem(props);
    }
  }

  public static mapToItem(abstractItem: ItemBase): Item {
    const props = abstractItem.getProperties();
    return new Item(props.name, props.sellIn, props.quality);
  }

  private static isConjured(props: ItemProperties) {
    return props.name.toUpperCase().includes(CONJURED.toUpperCase());
  }
}
