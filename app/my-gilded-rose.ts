import { ItemBase } from "./domain/item/item.base";
import { Item } from "./item.dto";
import { ItemFactory } from "./item.factory";

export class MyGildedRose {
  private readonly _baseItems: Array<ItemBase>;

  constructor(items = [] as Array<Item>) {
    this._baseItems = items.map(ItemFactory.of);
  }

  get items(): Array<Item> {
    return this._baseItems.map(ItemFactory.mapToItem);
  }

  updateQuality() {
    this._baseItems.forEach((item) => item.update());
    return this.items;
  }
}
